import string
def ispanagram(string):
    alphabet="abcdefghijklmnopqrstuvwxyz"
    for char in alphabet:
        if char not in string.lower():
            return False
    return  True
#drive code
string= 'abcd efgh ijkl nopqrs tuvwxyz'
if(ispanagram(string)==True):
    print("this string is panagram")
else:
    print("this string is not panagram")