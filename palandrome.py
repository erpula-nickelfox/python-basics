 #method 1:reverse a string
def method1(s):
 return s==s[::-1]
 #merhod2:Iterative
def method2(s):
 for i in  range(0,int(len(s)/2)):
     if s[i]!=s[len(s)-i-1]:
         return False
     else:
         return True

#method3:using inbuild method:reversed
def method3(s):
    rev=''.join(reversed(s))
    if rev==s:
        return True
    return False

 #main method
s=input("Enter Your string:")
#ans=method1(s)
#ans=method2(s)
ans=method3(s)
if(ans):
    print("yes string is palandrome")
else:
    print("No,string is not palandrome")

